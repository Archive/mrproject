/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Alvaro del Castillo <acs@barrapunto.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <glib.h>
#include <bonobo/bonobo-ui-component.h>
#include <bonobo/bonobo-ui-util.h>
#include <libgnome/gnome-i18n.h>
#include "app/mg-main-window.h"
#include "app/mg-plugin.h"
#include "mrproject/mrp-object.h"
#include "mrproject/mrp-project.h"
#include "mrproject/mrp-resource.h"
#include "mrproject/mrp-assignment.h"


void   plugin_init     (MgPlugin     *plugin,
                        MgMainWindow *main_window);
void   plugin_exit     (void);


static void project_resource_limit_cb (BonoboUIComponent *component,
					   gpointer           data, 
					   const char        *cname);
static void project_resource_limit    (MrpProject *project);
static void show_project_constraints      (MrpProject *project);


/* static void show_assignment               (MrpAssignment *mrp_assignment); */


static BonoboUIVerb verbs[] = {
	BONOBO_UI_VERB ("ResourceLimit", project_resource_limit_cb)
};

static BonoboUIComponent *ui_component;

static void
project_resource_limit_cb (BonoboUIComponent *component,
			       gpointer           data, 
			       const char        *cname)
{
	g_return_if_fail (MRP_IS_PROJECT (data));

	g_message ("Doing a resource limit\n");

	project_resource_limit (MRP_PROJECT(data));			
}

G_MODULE_EXPORT void 
plugin_exit (void) 
{
	g_message ("Test exit");
}

G_MODULE_EXPORT void 
plugin_init (MgPlugin     *plugin,
	     MgMainWindow *main_window)
{
	BonoboUIContainer *ui_container;
	MrpProject        *project;

	project = mg_main_window_get_project (main_window);

	ui_container = mg_main_window_get_ui_container (main_window);
	ui_component = bonobo_ui_component_new_default ();

	bonobo_ui_component_set_container (ui_component,
					   BONOBO_OBJREF (ui_container),
					   NULL);       

	bonobo_ui_component_freeze (ui_component, NULL);

	bonobo_ui_component_add_verb_list_with_data (ui_component,
						     verbs,
						     project);
	
	bonobo_ui_util_set_ui (ui_component,
 			       DATADIR,
 			       "/gnome-2.0/ui/GNOME_MrProject_ResourceLevellingPlugin.ui",
 			       "ResourceLevellingPlugin",
 			       NULL);

	bonobo_ui_component_thaw (ui_component, NULL);
}

static GList *
project_assignment_at_time (MrpProject *project,
			    mrptime     resources_time) 
{
	GList *l, *tasks;
	GList *assignments_done = NULL;

	tasks = mrp_project_get_all_tasks (project);
	
	for (l = tasks; l; l = l->next) {
		mrptime task_start = mrp_task_get_start (l->data);
		mrptime task_end = mrp_task_get_finish (l->data);

		if (task_end > resources_time && resources_time > task_start) {			
			GList *assignments = mrp_task_get_assignments (l->data);
			assignments_done = g_list_concat (assignments_done, g_list_copy(assignments));
		}
	}
	return assignments_done;	
}

/* static void 
show_assignment (MrpAssignment *mrp_assignment) 
{
	MrpResource *resource;
	gchar       *name;
	
	g_return_if_fail (MRP_IS_ASSIGNMENT(mrp_assignment));
	
	resource = mrp_assignment_get_resource (mrp_assignment);
	mrp_object_get (resource, "name", &name, NULL);
	g_message ("Need the resource: %s", name);		
} */

/* This function checks if we have enought resources for all the
   assignments */
static gboolean
project_assignments_check (MrpProject *project, 
			   GList      *assignments_needed) 
{
	GList *l, *all_resources;
	
	all_resources = g_list_copy(mrp_project_get_resources (project));

	for (l = assignments_needed; l ; l = l->next) {
		MrpResource *resource = mrp_assignment_get_resource (l->data);
		gchar *name;
		
		mrp_object_get (resource, "name", &name, NULL);
		
		if (g_list_find (all_resources, resource)) {
			/* g_message ("Find the resource: %s", name); */
			/* FIXME: When we use units, we have to change this and only
			   discount the units available */
			all_resources = g_list_remove (all_resources, resource);
		} else {
			/* g_message ("NOT Find the resource: %s", name); */
			return FALSE;			
		}				
	}
	return TRUE;	
}

static void
mg_project_task_move (MrpTask *task, gint delay) 
{
	mrptime start, finish;
	MrpConstraint *constraint;
	MrpProject    *project;
	gchar         *name;
	
	mrp_object_get (task, "name", &name, NULL);

	/* Not work ok and why property and also mrp_task_get_start/finish ? */
	/* mrp_object_get (task, "start", &start, "finish", &finish, NULL); */

	start = mrp_task_get_start (task);
	finish = mrp_task_get_finish (task);
	g_message ("Previous start %s: %s", name, mrp_time_to_string (start));
	start += delay;
	g_message ("New start %s: %s", name, mrp_time_to_string (start));
	finish += delay;

	mrp_object_get (task, "project", &project, NULL);
	mrp_object_get (task, "constraint", &constraint, NULL);
	constraint->type = MRP_CONSTRAINT_MSO;
	constraint->time = start;

	mrp_object_set (task,"constraint", constraint, NULL);
	/* This is done by the scheduler but if we do it here we can see how the
	   process advance in the GUI */
	/* mrp_object_set (task, "start", start, "finish", finish, NULL); */

	show_project_constraints (project);

	g_message ("Start %s: %s", name, mrp_time_to_string (start));
	g_message ("Finish %s: %s", name, mrp_time_to_string (finish));
	g_free (name);
}

static void
update_project_constraints (MrpProject *project) 
{
	GList *l, *tasks;

	tasks = mrp_project_get_all_tasks (project);
	
	for (l = tasks; l; l = l->next) {
		MrpTask       *task = l->data;
		MrpConstraint *constraint;
		mrptime        task_start = mrp_task_get_start (task);

		g_object_get (MRP_OBJECT (task),
			      "constraint", &constraint,
			      NULL);
		
		constraint->time = task_start;
		constraint->type = MRP_CONSTRAINT_MSO;
		   
		g_object_set (task, "constraint", constraint,  NULL); 
		
	}		
}

static gchar *
constraint_name (gint constraint) {
	gchar *name = NULL;

	switch (constraint) {
	case 	MRP_CONSTRAINT_ASAP:
		name = "as-soon-as-possible";
		break;
	case	MRP_CONSTRAINT_ALAP: 
		name="as-late-as-possible";
		break;
	case	MRP_CONSTRAINT_SNET: 
		name="start-no-earlier-than";
		break;
	case	MRP_CONSTRAINT_FNLT: 
		name="finish-no-later-than";
		break;
	case	MRP_CONSTRAINT_MSO:
		name="must-start-on";
		break;
	default:
		g_message ("Constraint not found: %d", constraint);
		g_assert_not_reached ();		
		break;
	}
	return name;
}

static void
show_project_constraints (MrpProject *project) 
{
	GList *l, *tasks;

	tasks = mrp_project_get_all_tasks (project);
	
	for (l = tasks; l; l = l->next) {
		gchar         *name;
		MrpTask       *task = l->data;
		MrpConstraint *constraint;
		mrptime        task_start = mrp_task_get_start (task);
		mrptime        task_finish = mrp_task_get_finish (task);

		mrp_object_get (MRP_OBJECT (task),
				"name", &name,
				NULL);

		mrp_object_get (MRP_OBJECT (task),
				 "constraint", &constraint,
				 NULL);
		
		g_message ("Task %s constraint %s time %s - start %s / finish %s", 
			   name, constraint_name (constraint->type),  
			   mrp_time_to_string (constraint->time),
			   mrp_time_to_string (task_start),
			   mrp_time_to_string (task_finish)); 

		g_free (name);
		
	}		
}




/* We need to free resources moving tasks
   We use an iterative approach so the criterium we use 
   can't change in time. I haven't think about how every approach
   don't change in time so be careful if you change this.
   Possible approaches: 

       1. We take the longest task and delay it one hour
       2. We move the task that takes more resources
       3. We move the task that cost less
       4. We move the task that takes more time to finish
       5. We move the task that has been in execution less time 
       6. We move the task that has the lower priority (rhult idea)

   We have to think about other solutions and dependencies and
   hierarchies in tasks
   
   For the moment we use approach 1

   And also we try not to move if possible critical path tasks
*/

static gboolean
project_assignments_free (GList *overload_assignments, gboolean try_critical)
{
	static   gboolean moved_critical = FALSE;
	GList   *l;
	MrpTask *task_move = NULL;
	gint     max_duration = 0;
	gint     delay = 3600;
	gint     task_critical;
	

	g_message ("\nLooking for task to move");

	for (l = overload_assignments; l; l = l->next) {
		gchar   *task_name;
		MrpTask *task = mrp_assignment_get_task (l->data);		
		gint     task_duration = mrp_task_get_duration (task);
		
		mrp_object_get (task, "critical", &task_critical, NULL);
		mrp_object_get (task, "name", &task_name, NULL);

		g_message ("Looking for move %s , critical %d, duration %d", 
			   task_name, task_critical, task_duration);

		/* If we have moved a critical task, we don't take care ot the
		 critical task flag to resolve loops deadlocks */

		if (!task_critical || try_critical || moved_critical) {
			if (task_duration >  max_duration) {
				max_duration = task_duration;
				task_move = task;				
			}			
		}
	}
		
	if (MRP_IS_TASK (task_move)) {
		gchar *name;
		g_object_get (task_move, "name",&name,NULL);
		g_message ("Moving task: %s", name); 

		mrp_object_get (task_move, "critical", &task_critical, NULL);
		if (task_critical) {
			g_message ("The task %s IS critical", name);
			moved_critical = TRUE;
		} else {			
			g_message ("The task %s IS NOT critical", name);
		}

		mg_project_task_move (task_move, delay);
		return TRUE;
	} else {
		return FALSE;
	}
}

static mrptime
project_get_finish (MrpProject *project) 
{
	GList   *tasks, *l;
	mrptime  project_end, task_end;
	
	tasks = mrp_project_get_all_tasks (project);

	project_end = mrp_project_get_project_start (project);

	for (l = tasks; l; l = l->next) {
		MrpTask *task = l->data;
		gchar   *name;
		
		task_end = mrp_task_get_finish (task);
		mrp_object_get (task, "name", &name, NULL);

		project_end = project_end > task_end ? project_end : task_end;
	}
	
	return project_end;
}

static void 
project_resource_limit (MrpProject *project) 
{
	GList    *tasks;
	mrptime   project_start, project_end, analysis_time;
	gint      limit = 0, interval = 3600, surrender_limit = 10;
	

	g_message ("Test init");

	tasks = mrp_project_get_all_tasks (project);

	if (g_list_length (tasks) == 0) {
		g_message (_("No task to resource limit"));
		return;		
	}
	if (1) {
		show_project_constraints (project);
	}

	project_start = mrp_project_get_project_start (project);

	project_end = mrp_task_get_finish (mrp_project_get_root_task (project));

	g_message ("Project end finish: %s", mrp_time_to_string (project_end));

	/* Why this doens't work? It prints project_end the same as project_start */
	/* g_message ("Project data interval: %s - %s", 
		   mrp_time_to_string (project_start), 
		   mrp_time_to_string (project_end)); */

	analysis_time = project_start;

	/* g_message ("Project start analysis: %s", mrp_time_to_string (analysis_time)); */
	/* We check for needed resources in every time point in project */
	while (project_end > analysis_time) {
		GList   *needed_assignments;
		gboolean move_critical = FALSE;

		limit = 0;

		needed_assignments = project_assignment_at_time (project, analysis_time); 
		while (!project_assignments_check (project, needed_assignments) 
		       && limit++ < surrender_limit) 
		{
			if (!project_assignments_free (needed_assignments, move_critical)) {
				g_message ("Impossible to find a solution without critical");
				limit = surrender_limit;
			}
			needed_assignments = 
				project_assignment_at_time 
				(project, analysis_time);
		}
		
		/* g_message ("Limit value: %i", limit); */
		if (limit >= surrender_limit) {
			move_critical = TRUE;
			limit = 0;
			while (!project_assignments_check (project, needed_assignments)
			       && limit++ < surrender_limit)
			{
				if (!project_assignments_free (needed_assignments, move_critical)) {
					g_message ("Impossible to find a solution with critical");
					limit = surrender_limit;
				};
				needed_assignments = 
					project_assignment_at_time 
					(project, analysis_time);
			}			
		}
			
		
		analysis_time += interval;
		/* root task isn't updated to reflect the new task durations :( */
		/* project_end = mrp_task_get_finish (mrp_project_get_root_task (project)); */
		project_end = project_get_finish (project);
	}

	if (limit >= surrender_limit) {
		g_warning ("We haven't found a complete solution :(");
	}

	if (0) {
		update_project_constraints (project);
	}
		
	/* g_message ("Project end analysis: %s", mrp_time_to_string (analysis_time)); */
}
