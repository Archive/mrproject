<?xml version="1.0" encoding="ISO-8859-1"?> 
<xsl:stylesheet version="1.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="xml" encoding="iso-8859-1" indent="yes"/>

<xsl:template match="project">
	<html> 
	<head> 
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" /> 
	<title>Proyecto <xsl:value-of select="@name" /></title> 
	</head> 
	<body> 
	  
	<table align="center" width="60%">
		<th><h1>Project <xsl:value-of select="@name" /></h1></th>
		<tr>
			<td>
				<h2>Tasks:</h2>
			<table align="center" width="90%" border="1">
				<tr>
					<td align="center"><b>Name</b></td>
					<td align="center"><b>Work</b></td>
					<td align="center"><b>Cost</b></td>
				</tr> 
	<xsl:for-each select="task">
				<tr>
					<td>
						<xsl:value-of select="@name" />
					</td>
					<td align="right">
						<xsl:value-of select="@work" />
					</td>
					<td align="right">
						<xsl:value-of select="@cost" />
					</td>
				</tr>
	</xsl:for-each>
			</table>
			</td>
		</tr>
		<tr>
			<td>
		<xsl:for-each select="task">
			<h3>Task <xsl:value-of select="@name" />:</h3>
			<h4>Resources:</h4>
			<table align="center" width="90%" border="1">
				<tr>
					<td align="center"><b>Name</b></td>
					<td align="center"><b>Cost</b></td>
				</tr> 
			<xsl:for-each select="resource">
				<tr>
					<td>
						<xsl:value-of select="@name" />
					</td>
					<td align="right">
						<xsl:value-of select="@cost" />
					</td>
				</tr>
			</xsl:for-each>
			</table>
		</xsl:for-each>
			</td>
		</tr>
	</table>
<p><br/></p>
	
	<table align="center" width="60%">
		<tr>
		<td><hr/></td>
		</tr>
	</table>
	<table align="center" width="60%">

		<tr>
		<td><h3>Project <xsl:value-of select="@name"/> - Total Cost:</h3></td> 
		<td align="right"><h3><xsl:value-of select="@cost"/></h3></td> 
		</tr>
	</table>
	</body> 
	</html> 

</xsl:template>


</xsl:stylesheet>
