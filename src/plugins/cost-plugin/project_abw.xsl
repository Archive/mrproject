<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="xml" encoding="iso-8859-1" indent="yes"/>


<xsl:template match="project">
<abiword xmlns="http://www.abisource.com/awml.dtd" xmlns:awml="http://www.abisource.com/awml.dtd" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:dc="http://purl.org/dc/elements/1.1/" version="1.0.2" fileformat="1.0" styles="unlocked">
<styles>
<s type="P" name="Heading 1" basedon="Normal" followedby="Normal" props="keep-with-next:1; margin-top:22pt; font-weight:bold; margin-bottom:3pt; font-family:Arial; font-size:17pt"/>
<s type="P" name="Normal" basedon="" followedby="Current Settings" props="font-family:Times New Roman; dom-dir:ltr; font-style:normal; margin-left:0pt; text-indent:0in; margin-top:0pt; text-position:normal; font-weight:normal; font-variant:normal; color:000000; text-decoration:none; line-height:1.000000; margin-bottom:0pt; text-align:left; margin-right:0pt; bgcolor:transparent; font-size:12pt; widows:2; font-stretch:normal"/>
<s type="P" name="Heading 2" basedon="Normal" followedby="Normal" props="keep-with-next:1; font-size:14pt; margin-bottom:0.0417in; line-height:1.000000; font-weight:bold; font-family:Arial; margin-top:0.3056in"/>
<s type="P" name="Heading 3" basedon="Normal" followedby="Normal" props="keep-with-next:1; margin-top:22pt; font-weight:bold; margin-bottom:3pt; font-family:Arial; font-size:12pt"/>
</styles>

<pagesize pagetype="A4" orientation="portrait" width="8.267717" height="11.692913" units="in" page-scale="1.000000"/>
<section>
<p style="Heading 1">                                Project <xsl:value-of select="@name"/></p>
<p style="Heading 2">Tasks:</p>
<p><c props="font-weight:bold; font-size:14pt">			Name		Work		Cost</c>
<xsl:for-each select="task">
<c props="font-size:14pt"><xsl:text>			</xsl:text><xsl:value-of select="@name" /><xsl:text>		</xsl:text><xsl:value-of select="@work" /><xsl:text>		</xsl:text><xsl:value-of select="@cost" /></c>
</xsl:for-each>
</p>
<xsl:for-each select="task">
	<p style="Heading 2">  Task <xsl:value-of select="@name" />:</p>
	<p style="Heading 3">    Resources:</p>
	<p><c props="font-weight:bold; font-size:14pt">			Name			Cost</c>
	<xsl:for-each select="resource">
	<c props="font-size:14pt"><xsl:text>			</xsl:text><xsl:value-of select="@name" /><xsl:text>			</xsl:text><xsl:value-of select="@cost" /></c>
	</xsl:for-each>
	</p>
</xsl:for-each>
<p>     _________________________________________________________________</p>
<p><c props="font-weight:bold">                        Project <xsl:value-of select="@name"/> - Total Cost:                                  <xsl:value-of select="@cost"/></c></p>
</section>
</abiword>
</xsl:template>


</xsl:stylesheet>
