

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Alvaro del Castillo <acs@barrapunto.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <glib.h>
#include <bonobo/bonobo-ui-component.h>
#include <bonobo/bonobo-ui-util.h>
#include "app/mg-main-window.h"
#include "app/mg-plugin.h"

#include <libxml/tree.h>
#include <libxml/encoding.h>

#include <libxml/xmlmemory.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>

xmlDocPtr documento;
xmlNodePtr xml_project, xml_task, xml_resource;


void   plugin_init     (MgPlugin     *plugin,
                        MgMainWindow *main_window);
void   plugin_exit     (void);

static gfloat project_cost_resource_cost (MrpResource       *resource,
					  MrpTask           *task);

static gfloat project_cost_task_cost     (MrpTask           *task);

static gfloat project_cost_compute       (MrpProject        *project, int format);

static void   project_cost_compute_cb_html    (BonoboUIComponent *component,
					  gpointer           data, 
					  const char        *cname);
static void   project_cost_compute_cb_abw    (BonoboUIComponent *component,
					  gpointer           data, 
					  const char        *cname);
enum {
	FORMAT_HTML,
	FORMAT_ABIWORD,
	FORMAT_DOCBOOK
};


static BonoboUIVerb verbs[] = {
	BONOBO_UI_VERB ("ComputeCost_html", project_cost_compute_cb_html),
	BONOBO_UI_VERB ("ComputeCost_abw", project_cost_compute_cb_abw)
};

static BonoboUIComponent *ui_component;

static void
project_cost_compute_cb_html (BonoboUIComponent *component,
			 gpointer           data, 
			 const char        *cname)
{
	g_return_if_fail (MRP_IS_PROJECT (data));

	project_cost_compute (data, FORMAT_HTML);
}

static void
project_cost_compute_cb_abw (BonoboUIComponent *component,
			 gpointer           data, 
			 const char        *cname)
{
	g_return_if_fail (MRP_IS_PROJECT (data));

	project_cost_compute (data, FORMAT_ABIWORD);
}

static gfloat 
project_cost_resource_cost (MrpResource *resource,
			    MrpTask     *task)
{
	char  *resource_name, *str_cost;
	gfloat cost; /*, cost_overtime;*/
	gfloat total_cost; /*total_cost_overtime;*/
	gint   work;

	mrp_object_get (resource, "name", &resource_name, NULL);
	mrp_object_get (resource, "cost", &cost, NULL);
/*	mrp_object_get (resource, "cost_overtime", &cost_overtime, NULL);*/

	/* Duration in seconds, cost in cost per hour  - acs */
	work = mrp_task_get_work (task);
	total_cost = work * cost / 3600;
/*	total_cost_overtime = (mrp_task_get_duration (task)/3600) * cost_overtime;*/
	str_cost = g_strdup_printf ("%.2f", total_cost);
	xml_resource = xmlNewNode (NULL, "resource");
	xmlSetProp (xml_resource, "name", resource_name);
	xmlSetProp (xml_resource, "cost", str_cost);
	xmlAddChild (xml_task, xml_resource);
	return total_cost;
}

static gfloat 
project_cost_task_cost (MrpTask *task) 
{
	GList  *resources, *l;
	gfloat  task_cost = 0;
	char  *task_name, *work, *cost;
	gint task_work;

	g_return_val_if_fail (MRP_IS_TASK (task), 0);

	mrp_object_get (task, "name", &task_name, NULL);
	xml_task = xmlNewNode (NULL, "task");
	task_work = mrp_task_get_work (task);
	resources = mrp_task_get_assigned_resources (task);
	for (l = resources; l; l = l->next) {
		task_cost += project_cost_resource_cost (l->data, task);
	}
	task_work = task_work / 28800;
	cost = g_strdup_printf ("%.2f", task_cost);
	xmlSetProp (xml_task, "name", task_name);
	work = g_strdup_printf ("%dd", task_work);
	xmlSetProp (xml_task, "work", work);
	cost = g_strdup_printf ("%.2f", task_cost);
	xmlSetProp (xml_task, "cost", cost);
	xmlAddChild (xml_project, xml_task);
	return task_cost;
}

G_MODULE_EXPORT void 
plugin_exit (void) 
{
}

G_MODULE_EXPORT void 
plugin_init (MgPlugin     *plugin,
	     MgMainWindow *main_window)
{
	BonoboUIContainer *ui_container;
	MrpProject        *project;

	project = mg_main_window_get_project (main_window);

	ui_container = mg_main_window_get_ui_container (main_window);
	ui_component = bonobo_ui_component_new_default ();

	bonobo_ui_component_set_container (ui_component,
					   BONOBO_OBJREF (ui_container),
					   NULL);       

	bonobo_ui_component_freeze (ui_component, NULL);

	bonobo_ui_component_add_verb_list_with_data (ui_component,
						     verbs,
						     project);
	
	bonobo_ui_util_set_ui (ui_component,
 			       DATADIR,
 			       "/gnome-2.0/ui/GNOME_MrProject_CostPlugin.ui",
 			       "CostPlugin",
 			       NULL);

	bonobo_ui_component_thaw (ui_component, NULL);
}

static gfloat 
project_cost_compute (MrpProject *project, int format) 
{
	GList             *tasks, *l;
	gfloat             project_cost = 0;
	gchar  *project_name, *cost;
	
	xsltStylesheetPtr cur = NULL;
	xmlDocPtr res;
	gchar *filename, *xslname;
	FILE *file;
	
	documento = xmlNewDoc ("1.0");

	/* Se crea el XML base de las estadisticas del proyecto */
	xml_project = xmlNewDocNode (documento, NULL, "project", NULL);
	xmlDocSetRootElement (documento, xml_project);
	
	
	g_object_get (project, "name", &project_name, NULL);
	xmlSetProp (xml_project, "name", project_name);
	tasks = mrp_project_get_all_tasks (project);
	for (l = tasks; l; l = l->next) {
		project_cost += project_cost_task_cost (l->data);
	}
	cost = g_strdup_printf ("%.2f", project_cost);
	xmlSetProp (xml_project, "cost", cost);
	/* Se aplica la hoja de estilo y se guarda en el formato adecuado */
	/*filename = g_strconcat(project_name,".html", NULL);*/
	switch (format) 
	{
		case FORMAT_HTML: 
			filename = g_strconcat("project",".html", NULL);
			xslname = g_strconcat(DATADIR "/mrproject/xsl/project","_html.xsl", NULL);
			break;
		case FORMAT_ABIWORD: 
			filename = g_strconcat("project",".abw", NULL);
			xslname = g_strconcat(DATADIR "/mrproject/xsl/project","_abw.xsl", NULL);
			break;
		default:
			filename = g_strconcat("project",".html", NULL);
			xslname = g_strconcat("project",".xsl", NULL);
	}
	file=fopen(filename,"w");
	
	cur = xsltParseStylesheetFile(xslname);
	res = xsltApplyStylesheet(cur, documento, NULL);
	xsltSaveResultToFile(file, res, cur);
	
	fclose(file);
	
	xsltFreeStylesheet(cur);
	xmlFreeDoc(documento);
	xmlFreeDoc(res);

	xsltCleanupGlobals();
	xmlCleanupParser();
	return project_cost;
}

