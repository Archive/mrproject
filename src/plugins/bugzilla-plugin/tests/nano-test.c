/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <string.h>
#include <glib.h>
#include <libxml2/libxml/nanohttp.h>
#include <libxml2/libxml/parser.h>
#define BUFSIZE 1024*30

char *flag;
GList *lista;

typedef struct tabla_s{
	gchar *tag;
	gchar *resultado;
} tabla;

void register_element(gchar *);


static void start_Doc             (void *);
static void end_Doc               (void *);
static void start_Element         (void *, const xmlChar *, const xmlChar **);
static void end_Element           (void *, const xmlChar *);
static void input_characters      (void *ctx, const xmlChar *ch, int len);

int main (int argc, char *argv[])
{
	void      *xml_handle;
	// gchar *url_bug_utf8 = "http://bugzilla.hispalinux.es/xml.cgi?id=500";
	// gchar     *url_bug = "http://bugzilla.gnome.org/xml.cgi?id=95827";
	gchar     *url_bug = "http://bugzilla.codefactory.se/xml.cgi?id=300";
	gchar      xml_res[BUFSIZE];
	int        n;
	tabla      *t_aux;
	GList      *aux;

	xmlSAXHandlerPtr sa = malloc (sizeof(xmlSAXHandler));
	initxmlDefaultSAXHandler (sa, 0);

	sa->startDocument = start_Doc;
	sa->endDocument = end_Doc;
	sa->startElement = start_Element;
	sa->endElement = end_Element;
	sa->characters = input_characters;

	register_element("bug_id");
	register_element("bug_status");

	bzero (xml_res,BUFSIZE);

	xmlNanoHTTPInit();

	xml_handle = xmlNanoHTTPOpen (url_bug, 0);

	n = xmlNanoHTTPRead (xml_handle, xml_res, sizeof(xml_res));
	if (n >= BUFSIZE){
		g_warning ("Buffer overrun");
		exit(0);
	}
	printf("\n\nxml_res: %s\n", xml_res);
        xmlSAXParseMemory (sa, xml_res ,sizeof(xml_res) , 0);

// YA tenemos los valores
	aux = g_list_first(lista);
	while(aux){
		t_aux = aux->data;
		g_message("%s -> %s",t_aux->tag, t_aux->resultado);
		aux = g_list_next(aux);
	}

	exit(0);
}

void register_element(gchar *tag)
{
	//Registramos los elementos en la lista
	tabla *t = (tabla *)malloc(sizeof(tabla));

	t->tag = (char *)malloc(strlen(tag));
	strncpy(t->tag, tag, strlen(tag));
	lista = g_list_append(lista, t);	
}

void start_Doc(void *ctx)
{
//	g_message("Init Doc");
	g_message("Inicializando parsing...");
}

void end_Doc(void *ctx)
{
//	g_message("End Doc");
	g_message("Parsing finalizado =)");
}

void start_Element(void *ctx, const xmlChar *name, const xmlChar **atts)
{
	tabla *t_aux;
	GList *aux;

	
	aux = lista;
//	lista = g_list_last(lista);
	while ( aux != NULL ) {
		t_aux = (tabla *)aux->data;
		if ( strcmp(name, t_aux->tag )== 0 ){
//			g_message("<%s>",t_aux->tag);
			flag = (char *) malloc(strlen(t_aux->tag)+1);
			bzero(flag,strlen(t_aux->tag));
			strcpy(flag, t_aux->tag);
			return;
		} else {
			aux = g_list_next(aux);
		}
	}
			      
/*
	if ( strcmp ( name , "bug_id") == 0 ){
		g_message("<%s>",name);
		flag=1;
	}
*/
}


void end_Element(void *ctx, const xmlChar *name)
{
	
//	tabla *t_aux;
	GList *aux;

	
	aux = lista;
//	lista = g_list_last(lista);
/*	while ( aux != NULL ) {
		t_aux = (tabla *)aux->data;
		if ( strcmp(name, t_aux->tag )== 0 ){
			g_message("</%s>",t_aux->tag);
			flag=0;
			return;
		} else {
			aux = g_list_next(aux);
		}
	}
*/
// Solo hay q comprobar si la etiqueta que nos llega es = que la que 
//estamos mirando		
	if ( flag ) // Si necesitamos mirar el final
		if ( strcmp(name, flag) == 0){
//			g_message("</%s>",name);
			flag=0;
			return;
		}


        /*
	  if ( strcmp ( name, "bug_id") == 0){
		g_message("</%s>",name);
		flag=0;
	  }
	*/
}

void input_characters (void *ctx, const xmlChar *ch, int len){
	char *mensaje;
	tabla *t_aux;
	GList *aux;

	if ( flag ){
		aux = g_list_first(lista);
		while(aux){
			t_aux = aux->data;
			if ( strcmp(t_aux->tag, flag ) == 0){
				mensaje = (char *)malloc(len+1);
				bzero(mensaje,len+1);
				strncpy(mensaje, ch, len);
	                	g_message(mensaje);
				t_aux->resultado = (char *)malloc(len+1);
				strcpy(t_aux->resultado, mensaje);
				return;
			} else {
				aux = g_list_next(aux);
			}
		}	

	}
}



/* gcc -Wall `pkg-config --libs --cflags libxml-2.0 glib` nano-test.c -o nano-test */
