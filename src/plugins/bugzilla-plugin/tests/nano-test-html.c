/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <string.h>
#include <glib.h>
#include <libxml2/libxml/nanohttp.h>
#include <libxml2/libxml/parser.h>
#include <libxml2/libxml/HTMLparser.h>
#define BUFSIZE 1024*60

char *flag;
GList *lista;

typedef struct tabla_s{
	gchar *tag;
	gchar *resultado;
} tabla;

void register_element(gchar *);


static void start_Doc             (void *ctx);
static void end_Doc               (void *ctx);
static void start_Element         (void *ctx, const xmlChar *name, const xmlChar **atts);
static void end_Element           (void *ctx, const xmlChar *name);
static void input_characters      (void *ctx, const xmlChar *ch, int len);

xmlSAXHandler bugzillaSAXHandlerStruct = {
    NULL, /* internalSubset */
    NULL, /* isStandalone */
    NULL, /* hasInternalSubset */
    NULL, /* hasExternalSubset */
    NULL, /* resolveEntity */
    NULL, /* getEntity */
    NULL, /* entityDecl */
    NULL, /* notationDecl */
    NULL, /* attributeDecl */
    NULL, /* elementDecl */
    NULL, /* unparsedEntityDecl */
    NULL, /* setDocumentLocator */
    start_Doc, /* startDocument */
    end_Doc, /* endDocument */
    start_Element, /* startElement */
    end_Element, /* endElement */
    NULL, /* reference */
    input_characters, /* characters */
    NULL, /* ignorableWhitespace */
    NULL, /* processingInstruction */
    NULL, /* comment */
    NULL, /* xmlParserWarning */
    NULL, /* xmlParserError */
    NULL, /* xmlParserError */
    NULL, /* getParameterEntity */
    NULL, /* cdataBlock */
    NULL, /* externalSubset */
    1
};

htmlSAXHandlerPtr bugzillaSAXHandler = &bugzillaSAXHandlerStruct;


int main (int argc, char *argv[])
{
	void             *html_handle;
	gchar            *url_bug = "http://bugzilla.codefactory.se/buglist.cgi?product=mrproject&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED";
	gchar             html_res[BUFSIZE];
	int               n;
	/* tabla            *t_aux;
	   GList            *aux; */
	/* htmlDocPtr        doc = NULL; */
	htmlParserCtxtPtr ctxt;

	bzero (html_res,BUFSIZE);

	xmlNanoHTTPInit();

	html_handle = xmlNanoHTTPOpen (url_bug, 0);

	n = xmlNanoHTTPRead (html_handle, html_res, sizeof(html_res));
	if (n >= BUFSIZE){
		g_warning ("Buffer overrun");
		exit(0);
	}
	g_print (html_res);
	ctxt = htmlCreatePushParserCtxt(bugzillaSAXHandler, NULL, html_res, sizeof(html_res), NULL, 0);
        htmlParseChunk (ctxt, html_res ,n, 0);

// YA tenemos los valores
	/* aux = g_list_first(lista);
	while(aux){
		t_aux = aux->data;
		g_message("%s -> %s",t_aux->tag, t_aux->resultado);
		aux = g_list_next(aux);
		} */

	exit(0);
}

void register_element(gchar *tag)
{
	//Registramos los elementos en la lista
	tabla *t = (tabla *)malloc(sizeof(tabla));

	t->tag = (char *)malloc(strlen(tag));
	strncpy(t->tag, tag, strlen(tag));
	lista = g_list_append(lista, t);	
}

void start_Doc(void *ctx)
{
//	g_message("Init Doc");
	g_message("Inicializando parsing...");
}

void end_Doc(void *ctx)
{
//	g_message("End Doc");
	g_message("Parsing finalizado =)");
}

void start_Element(void *ctx, const xmlChar *name, const xmlChar **atts)
{
	/* tabla *t_aux; */
	GList *aux;

	g_message ("Start element: %s", name);
	
	aux = lista;
	/*	lista = g_list_last(lista);
	while ( aux != NULL ) {
		t_aux = (tabla *)aux->data;
		if ( strcmp(name, t_aux->tag )== 0 ){
			g_message("<%s>",t_aux->tag);
			flag = (char *) malloc(strlen(t_aux->tag)+1);
			bzero(flag,strlen(t_aux->tag));
			strcpy(flag, t_aux->tag);
			return;
		} else {
			aux = g_list_next(aux);
		}
	}
	*/		      
	/*
	if ( strcmp ( name , "bug_id") == 0 ){
		g_message("<%s>",name);
		flag=1;
	}
	*/
}


void end_Element(void *ctx, const xmlChar *name)
{
	
	GList *aux;

	g_message ("End element: %s", name);

	
	aux = lista;
	
	if ( flag ) // Si necesitamos mirar el final
		if ( strcmp(name, flag) == 0){
			flag=0;
			return;
		}
}

void input_characters (void *ctx, const xmlChar *ch, int len){
	/*char *mensaje;
	tabla *t_aux;
	GList *aux;*/

	g_message ("Readed characters: %s", ch);

	/*	if ( flag ){
		aux = g_list_first(lista);
		while(aux){
			t_aux = aux->data;
			if ( strcmp(t_aux->tag, flag ) == 0){
				mensaje = (char *)malloc(len+1);
				bzero(mensaje,len+1);
				strncpy(mensaje, ch, len);
	                	g_message(mensaje);
				t_aux->resultado = (char *)malloc(len+1);
				strcpy(t_aux->resultado, mensaje);
				return;
			} else {
				aux = g_list_next(aux);
			}
		}	

		} */
}



/* gcc -Wall `pkg-config --libs --cflags libxml-2.0 glib` nano-test-html.c -o nano-test-html */
