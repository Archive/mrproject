/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Roberto Perez <hylian@jazzfree.com>
 * Copyright (C) 2002 Alvaro del Castillo <acs@barrapunto.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnome/gnome-i18n.h>
#include <libxml2/libxml/nanohttp.h>
#include <libxml2/libxml/parser.h>
#include <bonobo/bonobo-ui-component.h>
#include <bonobo/bonobo-ui-util.h>
#include "app/mg-main-window.h"
#include "app/mg-plugin.h"

#define BUFSIZE 1024*30    /* Max buffer size needed for a bug info */

struct _MgpBugzillaPriv {
	char        *flag;
	gboolean     debug;
	gboolean     read_cancel;
	gboolean     new_bugs_only;
	gint         bug_start, bug_end, bug_readed;
	guint        timer; 
	const gchar *bug_url;
	GList       *tags_list; /* Tags we seek in the bug */
	GList       *bugs_list; /* All the bugs list */
	MrpProject  *project;
	GList       *resources;
	GladeXML    *gui;
	GtkWidget   *bugs_tree_view;
	GtkWidget   *dialog_get_bugs;
	GtkWidget   *dialog_load_bugs;
};
typedef struct _MgpBugzillaPriv  MgpBugzillaPriv;

typedef struct _bug_entry {
	gchar *tag;
	gchar *result;
} bug_entry;

enum {
	COL_BUG_ID,
	COL_BUG_NAME,
	COL_BUG_PERSON,
	COL_BUG_IMPORT,
	NUM_COLS
};

void plugin_init                           (MgPlugin *plugin,
					    MgMainWindow *main_window);
void plugin_exit                           (void);


static void      project_bugzilla_cb       (BonoboUIComponent *component,
					    gpointer data,
					    const char *cname);

static void      bugzilla_cancel_cb        (GtkWidget  *button, 
					    GtkWidget  *dialog);
static void      bugzilla_download_bugs_cb (void);
static void      bugzilla_load_bugs_cb     (void);
static void      bugzilla_drop_bugs_cb     (void);
static void      bugzilla_get_bugs         (void);
static void      bugzilla_free_resources   (void);
static void      bugzilla_import_bug       (gchar *task_name, gchar *person);

/* Download thread method */
static void      bugs_read_callback        (gpointer callback_data);
static void      bug_read                  (gpointer callback_data);

/* SAX parse */
static void      start_Doc                 (void *);
static void      end_Doc                   (void *);
static void      start_Element             (void *, const xmlChar *, const xmlChar **);
static void      end_Element               (void *, const xmlChar *);
static void      input_characters          (void *ctx, const xmlChar *ch, int len);

/* misc utils */
static void          register_element         (gchar *);
static MrpResource*  get_resource             (const gchar *email);
static void          bugzilla_fill_bugs_view  (void);
static GtkTreeModel* bugzilla_fill_bugs_model (void);
static void          bugzilla_import_cell_toggled (GtkCellRendererText  *cell, 
						   gchar                *path_str,
						   GtkTreeModel         *model);
static gboolean      user_want_bug            (gchar *bug_id);


/* node list control */
static void      show_bug_list (bug_entry *data);
static void      free_bug_info (bug_entry *bug);


static BonoboUIVerb verbs[] = {
  BONOBO_UI_VERB ("Bugzilla Import", project_bugzilla_cb)
};

static BonoboUIComponent *ui_component;
xmlSAXHandlerPtr          sa;
MgpBugzillaPriv          *priv;

static 
void bugzilla_download_bugs_cb (void)
{
	bugzilla_get_bugs ();
}

static void
bug_read (gpointer callback_data)
{
	void      *xml_handle;
	gint       bytes_readed;
	gchar      xml_res[BUFSIZE];

	xml_handle = xmlNanoHTTPOpen (callback_data, 0);
	bzero (xml_res, BUFSIZE);
	bytes_readed = xmlNanoHTTPRead (xml_handle, xml_res, sizeof(xml_res));

	if (bytes_readed >= BUFSIZE){
		g_warning ("Buffer overrun");
		return;
	} else {
		GList     *bug_tags=0, *l=NULL;
		bug_entry *bug_field, *field;
		
		xmlSAXParseMemory (sa, xml_res, sizeof(xml_res) , 0);
		l = g_list_first (priv->tags_list);
		bug_tags = 0;
		while (l) {
			field = (bug_entry *) l->data;
			bug_field = (bug_entry *) g_malloc (sizeof (bug_entry));
			bug_field->tag = strdup (field->tag);
			bug_field->result = strdup (field->result);
			bug_tags = g_list_append (bug_tags, bug_field);
			l = g_list_next (l);
		}
		priv->bugs_list = g_list_append (priv->bugs_list, bug_tags);
	}
	if (priv->debug) g_message ("Read and processed the bug read");
	xmlNanoHTTPClose (xml_handle);
}

static void
bugs_read_callback (gpointer callback_data)
{
	gint   i;
	gchar *bug_url;

	priv->bug_readed = -1;

	if (priv->debug) 
		g_message ("URL %s, From bug %d to bug %d", priv->bug_url, priv->bug_start, priv->bug_end);

	for (i=priv->bug_start ; i<=priv->bug_end; i++) {
		if (priv->read_cancel) break;
		bug_url = g_strdup_printf ("%s/xml.cgi?id=%d", priv->bug_url, i);
		sprintf (bug_url, "%s/xml.cgi?id=%d", priv->bug_url, i);
		if (priv->debug) g_message ("BUG Id URL: %s",bug_url);
		bug_read (bug_url);
		priv->bug_readed = i;
		g_free (bug_url);
	}
	// If the import has been canceled we clean the resources in this thread
	if (priv->read_cancel) 
		bugzilla_free_resources ();
	g_thread_exit (0);	
}

static 
void bugzilla_import_bug (gchar *task_name, gchar *person)
{
	MrpTask      *task = NULL;
	MrpResource  *resource = NULL;
	gchar        *name;

	task = g_object_new (MRP_TYPE_TASK,
			     "work", -1,
			     NULL);	
	g_object_set (G_OBJECT (task), 
		      "name", task_name, NULL);
	mrp_project_insert_task (priv->project,
				 NULL,
				 -1,
				 task);
	
	name = (strtok(strdup (person),"@"));
	resource = get_resource (person);
	if (resource == NULL) {
		resource = g_object_new (MRP_TYPE_RESOURCE, NULL);
		g_object_set (G_OBJECT (resource), 
			      "email", person, NULL);
		g_object_set (G_OBJECT (resource), 
			      "name", name, NULL);
		mrp_project_add_resource (priv->project, resource);
		g_list_free (priv->resources);
		priv->resources = g_list_copy (mrp_project_get_resources (priv->project));
	}
	g_free (name);
		       
	mrp_resource_assign (resource, task, 100);	
}

static 
gboolean user_want_bug (gchar *bug_id)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;
	gboolean      import;
	gchar        *id;
	

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (priv->bugs_tree_view));
	gtk_tree_model_get_iter_first (model, &iter);

	do {
		gtk_tree_model_get (model, &iter, COL_BUG_ID, &id, 
				    COL_BUG_IMPORT, &import, -1);		
		if (strcmp (bug_id, id) == 0)
			return import;
	} while (gtk_tree_model_iter_next (model, &iter));

	return FALSE;
}

static 
void bugzilla_load_bugs_cb (void)
{
	GList           *l, *tags_bug;
	bug_entry       *t_aux;

	if (priv->debug) 
		g_message ("Loading the bugs in MrProject: from bug %d to bug %d",
		   priv->bug_start, priv->bug_end);

	
	for (l = g_list_first (priv->bugs_list); l; l = l->next) {
		gboolean import_bug = TRUE;
		gchar *task_name = NULL, *person = NULL, *task_status = NULL; 
		gchar *bug_id = NULL;
		tags_bug = l->data;
		while (tags_bug) {
			t_aux = (bug_entry *) tags_bug->data;
			if (strcmp("short_desc", t_aux->tag )== 0) {
				task_name = t_aux->result;
			} else if (strcmp("assigned_to", t_aux->tag)== 0) {
				person = t_aux->result;
			} else if (strcmp("bug_status", t_aux->tag)== 0) {
				task_status = t_aux->result;
				if (priv->new_bugs_only && strcmp (task_status, "NEW")!= 0)
				    import_bug = FALSE;
			} else if (strcmp("bug_id", t_aux->tag)== 0) {
				bug_id = t_aux->result;
				import_bug = user_want_bug (bug_id);
			} else {
				g_warning ("Tag not expected: %s %s", t_aux->tag, t_aux->result);
			}
			tags_bug = g_list_next (tags_bug);
		}
		if (import_bug) {
			bugzilla_import_bug (task_name, person);
			
		}
	}
	bugzilla_free_resources ();
	gtk_widget_destroy (priv->dialog_load_bugs);
}

static void
free_bug_info (bug_entry *bug)
{
	g_return_if_fail (bug != NULL);
	g_free (bug->tag);
	g_free (bug->result);
	g_free (bug);	
}

static void
bugzilla_free_resources (void)
{
	GList *l, *tags_bug;

	/* We need to free now the bugs list */
	/* This is a first try that doesn't work - acs */
	for (l = g_list_first (priv->bugs_list); l; l = l->next) {
		tags_bug = l->data;
		g_list_foreach (tags_bug, (GFunc) free_bug_info, NULL);
		g_list_free (tags_bug);
		tags_bug = NULL;
	}
	g_list_free (priv->bugs_list);
	priv->bugs_list = NULL;
	g_list_foreach (priv->tags_list, (GFunc) free_bug_info, NULL);
	g_list_free (priv->tags_list);
	priv->tags_list = NULL;

	/* Parse engine */
	g_free (sa);

	/* http engine */
	xmlNanoHTTPCleanup ();

	/* Timer */
	gtk_timeout_remove (priv->timer);	
}

static
MrpResource* get_resource (const gchar *email) 
{
	GList       *l;
	gchar       *resource_email;

	g_return_val_if_fail (email != NULL, NULL);

	for (l = priv->resources; l; l = l->next) {
		g_object_get (l->data, "email", &resource_email, NULL);
		if (!strcmp (email, resource_email)) {
			g_free (resource_email);
			return l->data;
		}
		g_free (resource_email);
        }
	return NULL;
}

static 
GtkTreeModel *bugzilla_fill_bugs_model (void)
{
	GtkListStore      *model;
	GtkTreeIter        iter;
	GList             *l;

	model = gtk_list_store_new (NUM_COLS,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_BOOLEAN);

	for (l = g_list_first (priv->bugs_list); l; l = l->next) {
		gchar *task_name=NULL, *task_person=NULL, *task_id=NULL;
		GList *task = l->data;
		gtk_list_store_append (model, &iter);
		while (task) {
			bug_entry *task_data = task->data;
			if (strcmp("short_desc", task_data->tag) == 0)
				task_name = task_data->result;
			else if (strcmp("assigned_to", task_data->tag) == 0) 
				task_person = task_data->result;
			else if (strcmp("bug_id", task_data->tag) == 0) 
				task_id = task_data->result;
			task = g_list_next (task);
		}
		gtk_list_store_set (model,
				    &iter,
				    COL_BUG_ID, task_id,
				    COL_BUG_NAME, task_name,
				    COL_BUG_PERSON, task_person,
				    COL_BUG_IMPORT, TRUE,
				    -1);
	}
	return (GtkTreeModel*) model;
}

static 
void bugzilla_fill_bugs_view (void)
{
	GtkTreeModel      *model;
	GtkCellRenderer   *cell;
	GtkTreeViewColumn *col;

	model = bugzilla_fill_bugs_model ();

	gtk_tree_view_set_model (GTK_TREE_VIEW (priv->bugs_tree_view), model);

	cell = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Id"),
							cell,
							"text", COL_BUG_ID,
							NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->bugs_tree_view), col);


	cell = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Task"),
							cell,
							"text", COL_BUG_NAME,
							NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->bugs_tree_view), col);

	cell = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Person"),
							cell,
							"text", COL_BUG_PERSON,
							NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->bugs_tree_view), col);

	cell = gtk_cell_renderer_toggle_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Import"),
							cell,
							"active", COL_BUG_IMPORT,
							NULL);	
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->bugs_tree_view), col);
	g_signal_connect (cell,
			  "toggled",
			  G_CALLBACK (bugzilla_import_cell_toggled),
			  model);	
}

static void  
bugzilla_import_cell_toggled (GtkCellRendererText  *cell, 
			      gchar                *path_str,
			      GtkTreeModel         *model)
{
	GtkTreePath  *path;
	GtkTreeIter   iter;
	GtkListStore *store;
	gint          column;
	gboolean      import;
	
	path = gtk_tree_path_new_from_string (path_str);
	store = (GtkListStore*) model;
	column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell),
						     "column"));
	gtk_tree_model_get_iter (model, &iter, path);

	gtk_tree_model_get (model, &iter, COL_BUG_IMPORT, &import, -1);
	gtk_list_store_set (store, &iter, COL_BUG_IMPORT, !import, -1);

	gtk_tree_path_free (path);
}

static
gboolean check_bugs (gpointer data)
{
	GtkWidget *progress;
	gint bar_inc, inc;

	progress = glade_xml_get_widget (priv->gui, "progress_loading");

	inc = 100/(priv->bug_end - priv->bug_start);
	bar_inc = (priv->bug_readed - priv->bug_start)*inc;
	
	gtk_progress_set_value ((GtkProgress *) progress, bar_inc);
	
	bar_inc = (int )(100 / (priv->bug_end - priv->bug_start));
	if (priv->bug_readed == priv->bug_end) {
		gtk_progress_set_value ((GtkProgress *) progress, 100);
		gtk_widget_destroy (priv->dialog_get_bugs);
		bugzilla_fill_bugs_view ();
		gtk_widget_show (priv->dialog_load_bugs);
		return FALSE;
	}
	else
		return TRUE;
}

static 
void bugzilla_get_bugs (void)
{
	GtkWidget *widget;
	gint       check_time = 1000; /* 1 second */

	xmlNanoHTTPInit();

	widget = glade_xml_get_widget (priv->gui, "bug_start");
	priv->bug_start = gtk_spin_button_get_value_as_int ((GtkSpinButton *) widget);
	widget = glade_xml_get_widget (priv->gui, "bug_end");
	priv->bug_end = gtk_spin_button_get_value_as_int ((GtkSpinButton *) widget);
	widget = glade_xml_get_widget (priv->gui, "bugzilla_url");
	priv->bug_url = gtk_entry_get_text ((GtkEntry *) widget);

	widget = glade_xml_get_widget (priv->gui, "bugzilla_get_ok");
	gtk_widget_set_sensitive (widget, FALSE);	
	widget = glade_xml_get_widget (priv->gui, "new_bugs_only");
	priv->new_bugs_only = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget));
	gtk_widget_set_sensitive (widget, FALSE);

	g_thread_create ((GThreadFunc) bugs_read_callback, NULL, FALSE, NULL);
	priv->timer = gtk_timeout_add (check_time, check_bugs, NULL); 
}

static 
void bugzilla_drop_bugs_cb (void)
{	
	gtk_widget_destroy (priv->dialog_load_bugs);
	bugzilla_free_resources ();	
}

static 
void  bugzilla_cancel_cb (GtkWidget  *button, 
			  GtkWidget  *dialog)
{
	priv->read_cancel = TRUE;
	gtk_widget_destroy (dialog);
}


static GtkWidget *
bugzilla_dialog_create (MrpProject *project)
{
	GtkWidget  *button;

	priv->gui = glade_xml_new (
		DATADIR "/mrproject/glade/mpp-bugzilla.glade", NULL , NULL);

	/* Get bugs dialog */
	priv->dialog_get_bugs = glade_xml_get_widget (priv->gui, "bugzilla_get"); 
	
	button = glade_xml_get_widget (priv->gui, "bugzilla_get_ok");
	g_signal_connect (button, "clicked",
			  G_CALLBACK (bugzilla_download_bugs_cb), 
			  priv->dialog_get_bugs);
	
	button = glade_xml_get_widget (priv->gui, "bugzilla_get_cancel");
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (bugzilla_cancel_cb), 
			  priv->dialog_get_bugs);

	/* Load bugs dialog */
	priv->dialog_load_bugs = glade_xml_get_widget (priv->gui, "bugzilla_load");
	button = glade_xml_get_widget (priv->gui, "bugzilla_load_ok");
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (bugzilla_load_bugs_cb), 
			  priv->dialog_load_bugs);
	button = glade_xml_get_widget (priv->gui, "bugzilla_load_cancel");
	g_signal_connect (button,
			  "clicked",
			  G_CALLBACK (bugzilla_drop_bugs_cb), 
			  priv->dialog_load_bugs);
	
	/* TreeView to show the bugs downloaded */
	priv->bugs_tree_view = glade_xml_get_widget (priv->gui, "bugzilla_bugs_treeview");

	return priv->dialog_get_bugs;
}

static void 
project_bugzilla_cb (BonoboUIComponent *component,
		     gpointer           data,
		     const char        *cname)
{
	sa = g_malloc (sizeof(xmlSAXHandler));
	initxmlDefaultSAXHandler (sa, 0);
	
	sa->startDocument = start_Doc;
	sa->endDocument = end_Doc;
	sa->startElement = start_Element;
	sa->endElement = end_Element;
	sa->characters = input_characters;
	
	register_element("bug_id");
	register_element("bug_status");
	register_element("assigned_to");
	register_element("short_desc");

	priv->read_cancel = FALSE;

	gtk_widget_show_all (bugzilla_dialog_create (NULL));
	
}

static
void show_bug_list (bug_entry *data) 
{
	g_message ("New list element: %s", data->tag);
}

void register_element(gchar *tag)
{
	bug_entry *t = (bug_entry *) g_malloc (sizeof(bug_entry));

	if (priv->debug) g_message ("New tag: %s", tag);
	t->result = NULL;
	t->tag = (char *) g_malloc(strlen(tag)+1);
	bzero (t->tag, strlen(t->tag));
	strncpy (t->tag, tag, strlen(tag));
	priv->tags_list = g_list_append (priv->tags_list, t);
	if (priv->debug) 
		g_list_foreach (priv->tags_list, (GFunc) show_bug_list, NULL);
}

void start_Doc(void *ctx)
{
}

void end_Doc(void *ctx)
{
}

void start_Element(void *ctx, const xmlChar *name, const xmlChar **atts)
{
	bug_entry *bug;
	GList     *l;

	l = g_list_first (priv->tags_list);
	while (l) {
		bug = (bug_entry *) l->data;
		if (strcmp(name, bug->tag) == 0){
			priv->flag = strdup (bug->tag);
			return;
		} else {
			l = g_list_next (l);
		}
	}
}

void end_Element(void *ctx, const xmlChar *name)
{
	
	GList *aux;
	
	aux = priv->tags_list;

	if ( priv->flag ) // If we are looking for the end tag
		if ( strcmp(name, priv->flag) == 0){
			priv->flag = 0;
			return;
		}
}

void input_characters (void *ctx, const xmlChar *ch, int len){
	bug_entry *bug;
	GList     *l;
	
	if (priv->flag){
		l = g_list_first (priv->tags_list);
		while(l){
			bug = l->data;
			if (strcmp(bug->tag, priv->flag ) == 0){
				bug->result = (char *) g_malloc(len+1);
				bzero (bug->result, len+1);
				strncpy (bug->result, ch, len);
				return;
			} else {
				l = g_list_next(l);
			}
		}	
		
	}
}

G_MODULE_EXPORT void
plugin_exit(void)
{
}


G_MODULE_EXPORT void
plugin_init (MgPlugin *plugin,
	     MgMainWindow *main_window)
{
	BonoboUIContainer *ui_container;
	
	priv = g_new0 (MgpBugzillaPriv, 1);
	priv->debug = TRUE;
	priv->new_bugs_only = FALSE;

	priv->project = mg_main_window_get_project (main_window);
	priv->resources = g_list_copy (mrp_project_get_resources (priv->project));

	ui_container = mg_main_window_get_ui_container (main_window);
	ui_component = bonobo_ui_component_new_default ();

	bonobo_ui_component_set_container (ui_component,
					   BONOBO_OBJREF(ui_container),
					   NULL);

	bonobo_ui_component_freeze (ui_component,NULL);

	bonobo_ui_component_add_verb_list_with_data (ui_component,
						     verbs,
						     priv->project);

	bonobo_ui_util_set_ui (ui_component,
			       DATADIR,
			       "/gnome-2.0/ui/GNOME_MrProject_BugzillaPlugin.ui",
			       "XmlPlugin",
			       NULL);

	bonobo_ui_component_thaw (ui_component, NULL);
}

