/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <time.h>
#include <ctype.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-program.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomecanvas/gnome-canvas.h>
#include <mrproject/mrp-time.h>
#include "mg-cell-renderer-date.h"
#include "mg-cell-renderer-list.h"

enum {
	COL_NAME,
	COL_START,
	COL_DURATION,
	NUM_COLS
};

GtkWidget    *tv;
GtkTreeModel *model;

static GList *
get_list (GtkCellRendererText *cell,
	  const gchar         *path_string,
	  gpointer             data)
{
	static GList *list = NULL;

	if (list != NULL) {
		return list;
	}

	list = g_list_append (NULL, "foo");
	list = g_list_append (list, "bar");
	list = g_list_append (list, "baz");
	list = g_list_append (list, "blabla");
	list = g_list_append (list, "yea yea");

	return list;
}

static gint
get_selected_index (GtkCellRendererText *cell,
		    const gchar         *path_string,
		    gpointer             data)
{
	return 2;
}

static void
edited (GtkCellRendererText *cell,
	gchar               *path_string,
	gchar               *new_text,
	gpointer             data)
{
	GtkTreeModel *model = GTK_TREE_MODEL (data);
	GtkTreePath  *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter   iter;
	gint          index;

	index = MG_CELL_RENDERER_LIST (cell)->selected_index;

	g_print ("%d %s\n", index, (gchar *) g_list_nth (get_list (NULL, NULL, NULL), index)->data);
	
	gtk_tree_model_get_iter (model, &iter, path);
	
	gtk_tree_path_free (path);
}

static void
start_edited (GtkCellRendererText *cell,
	      gchar               *path_string,
	      gchar               *new_text,
	      gpointer             data)
{
	GtkTreeModel *model = GTK_TREE_MODEL (data);
	GtkTreePath  *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter   iter;
	gchar        *str;
	mrptime       t;
	
	gtk_tree_model_get_iter (model, &iter, path);

	t = MG_CELL_RENDERER_DATE (cell)->time;
	str = g_strdup_printf ("%d %s", t.day, mrp_time_month_name (&t));
	g_print ("Start: %s, %s\n", new_text, str);
	g_free (str);
	
	gtk_tree_path_free (path);
}

static MrpConstraint *
get_constraint (GtkCellRendererText *cell,
		gchar               *path_string,
		gpointer             data)
{
	GtkTreeModel  *model = GTK_TREE_MODEL (data);
	GtkTreePath   *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter    iter;
	MrpConstraint *t;
	GValue         value = { 0 };

	gtk_tree_model_get_iter (model, &iter, path);

	gtk_tree_model_get_value (model,
				  &iter,
				  COL_START,
				  &value);

	t = g_value_get_boxed (&value);

	gtk_tree_path_free (path);

	return t;
}

static void
start_data_cb (GtkTreeViewColumn *tree_column,
	       GtkCellRenderer   *cell,
	       GtkTreeModel      *tree_model,
	       GtkTreeIter       *iter,
	       gpointer           data)
{
	GValue   value = { 0 };
	mrptime *t;
	gchar   *ret;

	gtk_tree_model_get_value (tree_model,
				  iter,
				  COL_START,
				  &value);

	t = g_value_get_boxed (&value);

	ret = g_strdup_printf ("%d %s", t->day, mrp_time_month_name (t));
	
	g_object_set (cell, "text", ret, NULL);
	g_value_unset (&value);
	g_free (ret);
}

static void
setup_tree_view (GtkTreeView *tree_view)
{
	GtkTreeViewColumn *col;
	GtkCellRenderer   *cell;
	
	gtk_tree_view_set_rules_hint (tree_view, TRUE);

	/*
	 * Name
	 */
	cell = mg_cell_renderer_list_new ();
	g_object_set (G_OBJECT (cell), "editable", TRUE, NULL);
	
	col = gtk_tree_view_column_new_with_attributes ("Name",
							cell,
							"text", COL_NAME,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	g_signal_connect (G_OBJECT (cell),
			  "edited",
			  G_CALLBACK (edited),
			  gtk_tree_view_get_model (tree_view));
	g_signal_connect (G_OBJECT (cell),
			  "get-list",
			  G_CALLBACK (get_list),
			  gtk_tree_view_get_model (tree_view));
	g_signal_connect (G_OBJECT (cell),
			  "get-selected-index",
			  G_CALLBACK (get_selected_index),
			  gtk_tree_view_get_model (tree_view));
	gtk_tree_view_append_column (tree_view, col);

	/*
	 * Start
	 */
	cell = mg_cell_renderer_date_new (TRUE);
	g_object_set (G_OBJECT (cell), "editable", TRUE, NULL);
	g_signal_connect (G_OBJECT (cell),
			  "edited",
			  G_CALLBACK (start_edited),
			  gtk_tree_view_get_model (tree_view));
	g_signal_connect (G_OBJECT (cell),
			  "get-constraint",
			  G_CALLBACK (get_constraint),
			  gtk_tree_view_get_model (tree_view));

	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, "Start");
	gtk_tree_view_column_set_resizable (col, TRUE);

	gtk_tree_view_column_pack_start (col, cell, TRUE);
	
	gtk_tree_view_column_set_cell_data_func (col,
						 cell,
						 start_data_cb,
						 tree_view,
						 NULL);
	
	gtk_tree_view_append_column (tree_view, col);

	/*
	 * Duration
	 */
/*	cell = gtk_cell_renderer_text_new ();
	g_object_set (G_OBJECT (cell), "editable", TRUE, NULL);
	col = gtk_tree_view_column_new_with_attributes ("Duration",
							cell,
							"text", COL_DURATION,
							NULL);
	gtk_tree_view_column_set_resizable (col, TRUE);
	gtk_tree_view_append_column (tree_view, col);
*/
}

#if 0
static time_t
time_from_isodate (const gchar *str)
{
	int       len;
	struct tm my_tm;
	time_t    t;
	int       i;

	g_return_val_if_fail (str != NULL, -1);

	/* yyyymmdd[Thhmmss[Z]] */

	len = strlen (str);

	if (!(len == 8 || len == 15 || len == 16))
		return -1;

	for (i = 0; i < len; i++)
		if (!((i != 8 && i != 15 && isdigit (str[i]))
		      || (i == 8 && str[i] == 'T')
		      || (i == 15 && str[i] == 'Z')))
			return -1;

	memset (&my_tm, 0, sizeof (my_tm));

#define digit_at(x,y) (x[y] - '0')

	my_tm.tm_year = (digit_at (str, 0) * 1000 + digit_at (str, 1) * 100 +
			 digit_at (str, 2) * 10 + digit_at (str, 3)) - 1900;

	my_tm.tm_mon  = digit_at (str, 4) * 10 + digit_at (str, 5) - 1;
	my_tm.tm_mday = digit_at (str, 6) * 10 + digit_at (str, 7);

	if (len > 8) {
		my_tm.tm_hour = digit_at (str, 9) * 10 + digit_at (str, 10);
		my_tm.tm_min  = digit_at (str, 11) * 10 + digit_at (str, 12);
		my_tm.tm_sec  = digit_at (str, 13) * 10 + digit_at (str, 14);
	}

	my_tm.tm_isdst = -1;

	t = mktime (&my_tm);

	if (len == 16) {
/*#if defined(HAVE_TM_GMTOFF)*/
		t += my_tm.tm_gmtoff;
/*#elif defined(HAVE_TIMEZONE)
  t -= timezone;
  #endif*/
	}
	    
	return t;
}
#endif

static GtkTreeModel*
create_model (void)
{
	GtkTreeStore *store;
        GtkTreeIter   iter;
	mrptime       t;
	
	store = gtk_tree_store_new (3,
				    G_TYPE_STRING,
				    MRP_TYPE_TIME,
				    G_TYPE_STRING);

	gtk_tree_store_insert (store,
			       &iter,
			       NULL,
			       0);

	t = mrp_time_new_from_time_t (time (NULL), TRUE);
	gtk_tree_store_set (store,
			    &iter,
			    0, "Test",
			    1, &t,
			    2, "2 days",
			    -1);

	gtk_tree_store_insert (store,
			       &iter,
			       NULL,
			       -1);

	t = mrp_time_new_from_time_t (time (NULL) + 60*60*24, TRUE);
	gtk_tree_store_set (store,
			    &iter,
			    0, "Test",
			    1, &t,
			    2, "2 days",
			    -1);

	return GTK_TREE_MODEL (store);
}

int
main (int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *vbox, *button;
	
        gnome_program_init ("test-popup", "0.1",
                            LIBGNOMEUI_MODULE,
                            argc, argv,
                            GNOME_PROGRAM_STANDARD_PROPERTIES,
                            NULL);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window), 650, 400);
	gtk_window_set_title (GTK_WINDOW (window), "Popup Test");

	vbox = gtk_vbox_new (FALSE, 0);

	model = create_model ();

	tv = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	setup_tree_view (GTK_TREE_VIEW (tv));

	gtk_box_pack_start (GTK_BOX (vbox), tv, TRUE, TRUE, 0);

	gtk_container_add (GTK_CONTAINER (window), vbox);

	button = gtk_button_new_with_mnemonic ("_Quit");
	g_signal_connect (G_OBJECT (button),
			  "clicked",
			  gtk_main_quit,
			  NULL);
	gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);

	g_signal_connect (G_OBJECT (window),
			  "destroy",
			  (GCallback) gtk_main_quit,
			  NULL);

	gtk_widget_show_all (window);
	
	gtk_main ();

	return 0;
}

