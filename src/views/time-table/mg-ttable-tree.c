#include <config.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtktreeselection.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkitemfactory.h>
#include <gtk/gtkiconfactory.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkmessagedialog.h>
#include <libgnome/gnome-i18n.h>

#include "util/mg-format.h"
#include "util/mg-marshal.h"
#include "cell-renderers/mg-cell-renderer-date.h"
#include "dialogs/task-dialog/mg-task-dialog.h"
#include "dialogs/resource-dialog/mg-resource-dialog.h"
//#include "dialogs/task-input-dialog/mg-task-input-dialog.h"
//#include "dialogs/property-dialog/mg-property-dialog.h"
#include "mg-ttable-tree.h"
#include "mg-ttable-model.h"

enum {
	SELECTION_CHANGED,
	EXPAND_ALL,
	COLLAPSE_ALL,
	LAST_SIGNAL
};

struct _MgTtableTreePriv {
	MrpProject	*project;
	MgMainWindow	*main_window;
	GHashTable	*task_dialogs;
	GHashTable	*resource_dialogs;

	GtkItemFactory	*popup_factory;
};

static void	 ttable_tree_class_init				(MgTtableTreeClass	*klass);
static void	 ttable_tree_init				(MgTtableTree		*tree);
static void	 ttable_tree_finalize				(GObject		*object);
static void	 ttable_tree_popup_edit_resource_cb		(gpointer		 callback_data,
								 guint			 action,
								 GtkWidget		*widget);
static void	 ttable_tree_popup_edit_task_cb			(gpointer		 callback_data,
								 guint			 action,
								 GtkWidget		*widget);
static void	 ttable_tree_popup_expand_all_cb		(gpointer		 callback_data,
								 guint			 action,
								 GtkWidget		*widget);
static void	 ttable_tree_popup_collapse_all_cb		(gpointer		 callback_data,
								 guint			 action,
								 GtkWidget		*widget);
static char	*ttable_tree_item_factory_trans			(const char		*path,
								 gpointer		 data);
static void	ttable_tree_tree_view_popup_menu		(GtkWidget		*widget,
								 MgTtableTree		*tree);
static gboolean	ttable_tree_tree_view_button_press_event	(GtkTreeView		*tree_view,
								 GdkEventButton		*event,
								 MgTtableTree		*tree);

static GtkTreeViewClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];

#define GIF_CB(x) ((GtkItemFactoryCallback)(x))

enum {
	POPUP_NONE,
	POPUP_REDIT,
	POPUP_TEDIT,
	POPUP_EXPAND,
	POPUP_COLLAPSE
};

static GtkItemFactoryEntry popup_menu_items[] = {
	{ N_("/_Edit resource..."),	NULL,	GIF_CB (ttable_tree_popup_edit_resource_cb),	POPUP_REDIT,	"<Item>",	NULL	},
	{ N_("/_Edit task..."),		NULL,	GIF_CB (ttable_tree_popup_edit_task_cb),	POPUP_TEDIT,	"<Item>",	NULL	},
	{ "/sep1",			NULL,	0,						POPUP_NONE,	"<Separator>" },
	{ N_("/_Expand all resources"),	NULL,	GIF_CB (ttable_tree_popup_expand_all_cb),	POPUP_EXPAND,	"<Item>",	NULL	},
	{ N_("/_Collapse all resources"), NULL,	GIF_CB (ttable_tree_popup_collapse_all_cb),	POPUP_COLLAPSE,	"<Item>",	NULL	},
};

GType
mg_ttable_tree_get_type(void) {
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgTtableTreeClass),
			NULL,           /* base_init */
			NULL,           /* base_finalize */
			(GClassInitFunc) ttable_tree_class_init,
			NULL,           /* class_finalize */
			NULL,           /* class_data */
			sizeof (MgTtableTree),
			0,              /* n_preallocs */
			(GInstanceInitFunc) ttable_tree_init
		};
		type = g_type_register_static (GTK_TYPE_TREE_VIEW, "MgTtableTree",
					       &info, 0);
	}
	return type;
}

static void
ttable_tree_class_init (MgTtableTreeClass *klass)
{
	GObjectClass *o_class;

	parent_class = g_type_class_peek_parent (klass);

	o_class = (GObjectClass *) klass;

	o_class->finalize = ttable_tree_finalize;

	signals[SELECTION_CHANGED] =
		g_signal_new ("selection-changed",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				0,
				NULL, NULL,
				mg_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	signals[EXPAND_ALL] =
		g_signal_new ("expand-all",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				0,
				NULL, NULL,
				mg_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	
	signals[COLLAPSE_ALL] =
		g_signal_new ("collapse-all",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				0,
				NULL, NULL,
				mg_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	
}

static void
ttable_tree_init (MgTtableTree *tree)
{
	MgTtableTreePriv *priv;

	priv = g_new0(MgTtableTreePriv,1);
	tree->priv = priv;

	priv->popup_factory = gtk_item_factory_new (
			GTK_TYPE_MENU,
			"<main>",
			NULL);
	gtk_item_factory_set_translate_func (
			priv->popup_factory,
			ttable_tree_item_factory_trans,
			NULL,
			NULL);
	gtk_item_factory_create_items (
			priv->popup_factory,
			G_N_ELEMENTS (popup_menu_items),
			popup_menu_items,
			tree);
}

static void
ttable_tree_finalize (GObject *object)
{
	MgTtableTree		*tree;
	MgTtableTreePriv	*priv;
	tree = MG_TTABLE_TREE(object);
	priv = tree->priv;
	g_free(priv);
	if (G_OBJECT_CLASS (parent_class)->finalize) {
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
	}
}

void
mg_ttable_tree_set_model (MgTtableTree  *tree,
		          MgTtableModel *model)
{
	gtk_tree_view_set_model (GTK_TREE_VIEW (tree),
				 GTK_TREE_MODEL (model));
	gtk_tree_view_expand_all (GTK_TREE_VIEW (tree));
}

static void
ttable_tree_setup_tree_view (GtkTreeView   *gtk_tree,
			     MrpProject    *project,
			     MgTtableModel *model)
{
	MgTtableTree     *tree;
//	GtkTreeSelection *selection;

	tree = MG_TTABLE_TREE(gtk_tree);

	mg_ttable_tree_set_model(tree,model);

	gtk_tree_view_set_rules_hint (gtk_tree, TRUE);
	gtk_tree_view_set_reorderable (gtk_tree, TRUE);

	g_signal_connect (gtk_tree,
			  "popup_menu",
			  G_CALLBACK (ttable_tree_tree_view_popup_menu),
			  gtk_tree);
	g_signal_connect (gtk_tree,
			  "button_press_event",
			  G_CALLBACK (ttable_tree_tree_view_button_press_event),
			  gtk_tree);
	
}

static void
ttable_tree_resname_data_func (GtkTreeViewColumn *tree_column,
			       GtkCellRenderer   *cell,
			       GtkTreeModel      *tree_model,
			       GtkTreeIter       *iter,
			       gpointer           data)
{
	gchar *name;
	gtk_tree_model_get (tree_model,
			    iter,
			    COL_RESNAME,&name,
			    -1);
	g_object_set (cell,
		      "text", name,
		      NULL);
	g_free(name);
}

static void
ttable_tree_taskname_data_func (GtkTreeViewColumn *tree_column,
			        GtkCellRenderer   *cell,
			        GtkTreeModel      *tree_model,
			        GtkTreeIter       *iter,
			        gpointer           data)
{
	gchar *name;
	gtk_tree_model_get (tree_model,
			    iter,
			    COL_TASKNAME,&name,
			    -1);
	g_object_set (cell,
		      "text", name,
		      NULL);
	g_free(name);
}

static void
ttable_tree_add_column(GtkTreeView *tree,
		       gint         column,
		       const gchar *title)
{
	GtkTreeViewColumn *col;
	GtkCellRenderer   *cell;

	switch (column) {
		case COL_RESNAME:
			cell = gtk_cell_renderer_text_new ();
			g_object_set (cell, "editable", FALSE, NULL);
			col = gtk_tree_view_column_new_with_attributes (title,cell,NULL);
			gtk_tree_view_column_set_cell_data_func (col,cell,
					ttable_tree_resname_data_func,NULL,NULL);
			g_object_set_data (G_OBJECT (col),"data-func",
					ttable_tree_resname_data_func);
			gtk_tree_view_column_set_resizable (col, TRUE);
			gtk_tree_view_column_set_min_width (col, 100);
			gtk_tree_view_append_column (tree, col);
			break;
		case COL_TASKNAME:
			cell = gtk_cell_renderer_text_new ();
			g_object_set (cell, "editable", FALSE, NULL);
			col = gtk_tree_view_column_new_with_attributes (title,cell,NULL);
			gtk_tree_view_column_set_cell_data_func (col,cell,
					ttable_tree_taskname_data_func,NULL,NULL);
			g_object_set_data (G_OBJECT (col),"data-func",
					ttable_tree_taskname_data_func);
			gtk_tree_view_column_set_resizable (col, TRUE);
			gtk_tree_view_column_set_min_width (col, 100);
			gtk_tree_view_append_column (tree, col);
			break;
		case COL_RESOURCE:
		case COL_ASSIGNMENT:
		default:
			break;
	}
}

GtkWidget *
mg_ttable_tree_new (MgMainWindow  *main_window,
		    MgTtableModel *model)
{
	MrpProject		*project;
	MgTtableTree		*tree;
	MgTtableTreePriv	*priv;
//	va_list			 args;
//	gpointer		 str;
//	gint			 col;

	tree = g_object_new (MG_TYPE_TTABLE_TREE, NULL);

	project = mg_main_window_get_project (main_window);

	priv = tree->priv;

	priv->main_window = main_window;
	priv->project = project;

	ttable_tree_setup_tree_view (GTK_TREE_VIEW (tree), project, model);

	ttable_tree_add_column (GTK_TREE_VIEW (tree), COL_RESNAME, _("\nName"));
	ttable_tree_add_column (GTK_TREE_VIEW (tree), COL_TASKNAME, _("\nTask"));

	return GTK_WIDGET(tree);
}






/* For the popup menu, and associated actions */
static void
ttable_tree_popup_edit_resource_cb	(gpointer	 callback_data,
					 guint		 action,
					 GtkWidget	*widget)
{
	mg_ttable_tree_edit_resource(callback_data);
}

static void
ttable_tree_popup_edit_task_cb		(gpointer	 callback_data,
					 guint		 action,
					 GtkWidget	*widget)
{
	mg_ttable_tree_edit_task(callback_data);
}

static void
ttable_tree_popup_expand_all_cb		(gpointer	 callback_data,
					 guint		 action,
					 GtkWidget	*widget)
{
	g_signal_emit(callback_data,signals[EXPAND_ALL],0);
//	gtk_tree_view_expand_all(callback_data);
}

void
mg_ttable_tree_expand_all	(MgTtableTree	*tree)
{
	g_return_if_fail(MG_IS_TTABLE_TREE(tree));
	gtk_tree_view_expand_all(GTK_TREE_VIEW(tree));
}

void
mg_ttable_tree_collapse_all	(MgTtableTree	*tree)
{
	g_return_if_fail(MG_IS_TTABLE_TREE(tree));
	gtk_tree_view_collapse_all(GTK_TREE_VIEW(tree));
}

static void
ttable_tree_popup_collapse_all_cb	(gpointer	 callback_data,
					 guint		 action,
					 GtkWidget	*widget)
{
	g_signal_emit(callback_data,signals[COLLAPSE_ALL],0);
//	gtk_tree_view_collapse_all(callback_data);
}

void
mg_ttable_tree_edit_resource	(MgTtableTree	*tree)
{
	MgTtableTreePriv	*priv;
	MrpResource		*resource;
	MrpAssignment		*assignment;
	GtkWidget		*dialog;
	GList			*list;

	g_return_if_fail(MG_IS_TTABLE_TREE(tree));

	priv = tree->priv;

	list = mg_ttable_tree_get_selected_items(tree);
	if (list==NULL)
		return;

	if (MRP_IS_RESOURCE(list->data)) {
		resource = MRP_RESOURCE(list->data);
	} else {
		assignment = MRP_ASSIGNMENT(list->data);
		resource = mrp_assignment_get_resource(assignment);
	}

	dialog = mg_resource_dialog_new (priv->main_window, resource);
	gtk_widget_show (dialog);
	g_list_free(list);
}

void
mg_ttable_tree_edit_task	(MgTtableTree	*tree)
{
	MgTtableTreePriv	*priv;
	MrpAssignment		*assignment;
	MrpTask			*task;
	GtkWidget		*dialog;
	GList			*list, *l;

	g_return_if_fail(MG_IS_TTABLE_TREE(tree));

	priv = tree->priv;

	list = mg_ttable_tree_get_selected_items(tree);
	if (list==NULL)
		return;

	assignment = NULL;
	l = list;
	while (l != NULL && assignment == NULL) {
		if (MRP_IS_ASSIGNMENT(l->data)) {
			assignment = MRP_ASSIGNMENT(l->data);
		} else {
			l = l->next;
		}
	}
	if (assignment == NULL)
		return;
	task = mrp_assignment_get_task(assignment);

	dialog = mg_task_dialog_new(priv->main_window, task);
	gtk_widget_show(dialog);
	g_list_free(list);
}

static void
ttable_tree_get_selected_func	(GtkTreeModel	*model,
				 GtkTreePath	*path,
				 GtkTreeIter	*iter,
				 gpointer	 data)
{
	GList		**list = data;
	MrpAssignment	 *assignment;
	MrpResource	 *resource;

	gtk_tree_model_get (model, iter,
			COL_ASSIGNMENT, &assignment,
			COL_RESOURCE, &resource,
			-1);
	if (assignment != NULL) {
		*list = g_list_prepend(*list,assignment);
	} else if (resource != NULL) {
		*list = g_list_prepend(*list,resource);
	} else {
		fprintf(stderr,"MgTtableTree: Ni resource ni assignment dans la selection, va comprendre\n");
	}
}

GList*
mg_ttable_tree_get_selected_items	(MgTtableTree	*tree)
{
	GtkTreeSelection	*selection;
	GList			*list;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));

	list = NULL;

	gtk_tree_selection_selected_foreach (
			selection,
			ttable_tree_get_selected_func,
			&list);
	list = g_list_reverse (list);
	return list;
}

static void
ttable_tree_tree_view_popup_menu	(GtkWidget	*widget,
					 MgTtableTree	*tree)
{
	gint	x, y;

	gdk_window_get_pointer (widget->window, &x, &y, NULL);

	gtk_item_factory_popup (tree->priv->popup_factory,
			x, y, 0,
			gtk_get_current_event_time ());
}

static gboolean
ttable_tree_tree_view_button_press_event	(GtkTreeView	*tree_view,
						 GdkEventButton	*event,
						 MgTtableTree	*tree)
{
	GtkTreePath		*path;
	GtkTreeView		*tv;
	MgTtableTreePriv	*priv;
	MgTtableModel		*model;
	GtkItemFactory		*factory;

	tv = GTK_TREE_VIEW(tree);
	priv = tree->priv;
	factory = priv->popup_factory;
	model = MG_TTABLE_MODEL(gtk_tree_view_get_model(tv));

	if (event->button == 3) {
		gtk_widget_grab_focus (GTK_WIDGET (tree));
		gtk_widget_set_sensitive (
				gtk_item_factory_get_widget_by_action (factory, POPUP_EXPAND), TRUE);
		gtk_widget_set_sensitive (
				gtk_item_factory_get_widget_by_action (factory, POPUP_COLLAPSE), TRUE);

		if (gtk_tree_view_get_path_at_pos (tv, event->x, event->y, &path, NULL, NULL, NULL)) {
			gtk_tree_selection_unselect_all (gtk_tree_view_get_selection (tv));
			gtk_tree_selection_select_path (gtk_tree_view_get_selection (tv), path);
			if (mg_ttable_model_path_is_assignment(model,path)) {
				gtk_widget_set_sensitive (
						gtk_item_factory_get_widget_by_action (factory, POPUP_REDIT), TRUE);
				gtk_widget_set_sensitive (
						gtk_item_factory_get_widget_by_action (factory, POPUP_TEDIT), TRUE);
			} else {
				gtk_widget_set_sensitive (
						gtk_item_factory_get_widget_by_action (factory, POPUP_REDIT), TRUE);
				gtk_widget_set_sensitive (
						gtk_item_factory_get_widget_by_action (factory, POPUP_TEDIT), FALSE);
			}
			gtk_tree_path_free (path);
		}
		gtk_item_factory_popup (factory, event->x_root, event->y_root,
				event->button, event->time);
		return TRUE;
	}
	return FALSE;
}

static char *
ttable_tree_item_factory_trans	(const char *path, gpointer data)
{
	return _((gchar*)path);
}
