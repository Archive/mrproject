/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MG_SCALE_UTILS_H__
#define __MG_SCALE_UTILS_H__

#include <mrproject/mrp-time.h>

/* Hardcode Monday (1, Sunday is 0) for now. */
#define START_OF_WEEK 1

typedef enum {
	MG_SCALE_UNIT_NONE,
	
	MG_SCALE_UNIT_YEAR,     /* Short: N/A     Medium: 2002      Long: N/A         */

	MG_SCALE_UNIT_HALFYEAR, /* Short: N/A     Medium: H1        Long: 2002, H1    */

	MG_SCALE_UNIT_QUARTER,  /* Short: Q1      Medium: N/A       Long: 2002, Qtr 1 */

	MG_SCALE_UNIT_MONTH,    /* Short: J       Medium: Jan       Long: Jan '02     */

	MG_SCALE_UNIT_WEEK,     /* Short: Wk 19   Medium: Week 19   Long: Week 19 '02 */

	MG_SCALE_UNIT_DAY,      /* Short: 1       Medium: Mon 1     Long: Mon, Apr 1  */

	MG_SCALE_UNIT_HALFDAY,  /* Short: 1       Medium: 1         Long: 1           */

	MG_SCALE_UNIT_TWO_HOURS,/* Short: 1       Medium: 1         Long: 1           */

	MG_SCALE_UNIT_HOUR      /* Short: 1       Medium: 1         Long: 1           */
} MgScaleUnit;

typedef enum {
	MG_SCALE_FORMAT_SHORT,
	MG_SCALE_FORMAT_MEDIUM,
	MG_SCALE_FORMAT_LONG
} MgScaleFormat;

typedef struct {
	MgScaleUnit   major_unit;
	MgScaleFormat major_format;
	
	MgScaleUnit   minor_unit;
	MgScaleFormat minor_format;

	/* Nonworking intervals shorter than this is not drawn (seconds). */
	gint          nonworking_limit;
} MgScaleConf;

extern const MgScaleConf *mg_scale_conf;

mrptime mg_scale_time_prev   (mrptime        t,
			      MgScaleUnit    unit);

mrptime mg_scale_time_next   (mrptime        t,
			      MgScaleUnit    unit);

gchar * mg_scale_format_time (mrptime        t,
			      MgScaleUnit    unit,
			      MgScaleFormat  format);

gint    mg_scale_clamp_zoom  (gdouble        zoom);


#endif /* __MG_SCALE_UTILS_H__ */
