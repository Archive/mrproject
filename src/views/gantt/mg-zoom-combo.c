/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <gtk/gtkcombo.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtklistitem.h>
#include "mg-zoom-combo.h"


enum {
	ZOOM_SELECTED,
	LAST_SIGNAL
};

static void     mzc_class_init           (MgZoomComboClass *klass);
static void     mzc_init                 (MgZoomCombo      *combo);
static void     mzc_finalize             (GObject          *object);
static void     mzc_add_item             (GtkCombo         *combo,
					  const gchar      *label,
					  gint              zoom);
static void     mzc_select_child_cb      (GtkWidget        *list,
					  GtkWidget        *child,
					  gpointer          data);
static void     mzc_activate_cb          (GtkWidget        *entry,
					  gpointer          data);


static GtkComboClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];


GType
mg_zoom_combo_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (MgZoomComboClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) mzc_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (MgZoomCombo),
			0,              /* n_preallocs */
			(GInstanceInitFunc) mzc_init
		};

		type = g_type_register_static (GTK_TYPE_COMBO, "MgZoomCombo",
					       &info, 0);
	}
	
	return type;
}

static void
mzc_class_init (MgZoomComboClass *class)
{
	GObjectClass *o_class;

	parent_class = g_type_class_peek_parent (class);

	o_class = (GObjectClass *) class;

	o_class->finalize = mzc_finalize;
	
	signals[ZOOM_SELECTED] = g_signal_new (
		"zoom_selected",
		G_TYPE_FROM_CLASS (class),
		G_SIGNAL_RUN_LAST,
		0,
		NULL, NULL,
		g_cclosure_marshal_VOID__INT,
		G_TYPE_NONE, 1, G_TYPE_INT);
}

static void
mzc_init (MgZoomCombo *combo)
{
	gtk_entry_set_width_chars (GTK_ENTRY (GTK_COMBO (combo)->entry), 6);

	gtk_button_set_relief (GTK_BUTTON (GTK_COMBO (combo)->button), GTK_RELIEF_NONE);

	gtk_combo_set_value_in_list (GTK_COMBO (combo), TRUE, FALSE);
	gtk_combo_disable_activate (GTK_COMBO (combo));
	
	mzc_add_item (GTK_COMBO (combo), "200%", 200);
	mzc_add_item (GTK_COMBO (combo), "100%", 100);
	mzc_add_item (GTK_COMBO (combo), "75%", 75);
	mzc_add_item (GTK_COMBO (combo), "50%", 50);
	mzc_add_item (GTK_COMBO (combo), "25%", 25);

	g_signal_connect (GTK_COMBO (combo)->list,
			  "select-child",
			  G_CALLBACK (mzc_select_child_cb),
			  combo);
	
	g_signal_connect (GTK_COMBO (combo)->entry,
			  "activate",
			  G_CALLBACK (mzc_activate_cb),
			  combo);
}

static void
mzc_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize) {
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
	}
}

static void
mzc_select_child_cb (GtkWidget *list, GtkWidget *child, gpointer data)
{
	gint zoom;

	zoom = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (child), "zoom"));

	g_signal_emit (data, signals[ZOOM_SELECTED], 0, zoom);
}

static void
mzc_activate_cb (GtkWidget *entry, gpointer data)
{
	gint zoom;

	zoom = atoi (gtk_entry_get_text (GTK_ENTRY (entry)));

	g_signal_emit (data, signals[ZOOM_SELECTED], 0, zoom);
}

static void
mzc_add_item (GtkCombo * combo, const gchar *label, gint zoom)
{
	GtkWidget *item;
	
	item = gtk_list_item_new_with_label (label);
	gtk_widget_show (item);
	gtk_container_add (GTK_CONTAINER (combo->list), item);
	g_object_set_data (G_OBJECT (item), "zoom", GINT_TO_POINTER (zoom));
}

GtkWidget *
mg_zoom_combo_new (void)
{
	GtkWidget *combo;

	combo = g_object_new (MG_TYPE_ZOOM_COMBO, NULL);
	
	return combo;
}

