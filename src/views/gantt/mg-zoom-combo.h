/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MG_ZOOM_COMBO_H__
#define __MG_ZOOM_COMBO_H__

#include <gtk/gtkcombo.h>

#define MG_TYPE_ZOOM_COMBO		(mg_zoom_combo_get_type ())
#define MG_ZOOM_COMBO(obj)		(GTK_CHECK_CAST ((obj), MG_TYPE_ZOOM_COMBO, MgZoomCombo))
#define MG_ZOOM_COMBO_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), MG_TYPE_ZOOM_COMBO, MgZoomComboClass))
#define MG_IS_ZOOM_COMBO(obj)		(GTK_CHECK_TYPE ((obj), MG_TYPE_ZOOM_COMBO))
#define MG_IS_ZOOM_COMBO_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), MG_TYPE_ZOOM_COMBO))
#define MG_ZOOM_COMBO_GET_CLASS(obj)	(GTK_CHECK_GET_CLASS ((obj), MG_TYPE_ZOOM_COMBO, MgZoomComboClass))

typedef struct _MgZoomCombo           MgZoomCombo;
typedef struct _MgZoomComboClass      MgZoomComboClass;

struct _MgZoomCombo
{
	GtkCombo       parent;
};

struct _MgZoomComboClass
{
	GtkComboClass  parent_class;
};


GType      mg_zoom_combo_get_type (void);

GtkWidget *mg_zoom_combo_new      (void);

#endif /* __MG_ZOOM_COMBO_H__ */
