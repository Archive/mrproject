/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003 CodeFactory AB
 * Copyright (C) 2003 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2003 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MG_GANTT_PRINT_JOB_H__
#define __MG_GANTT_PRINT_JOB_H__

#include <gtk/gtktreeview.h>
#include "src/util/mg-print-job.h"
#include "src/app/mg-view.h"

typedef struct _MgGanttPrintData MgGanttPrintData;


void                mg_gantt_print_do             (MgGanttPrintData *data);

gint                mg_gantt_print_get_n_pages    (MgGanttPrintData *data);

MgGanttPrintData *  mg_gantt_print_data_new       (MgView           *view,
						   MgPrintJob       *job,
						   GtkTreeView      *tree_view,
						   gint              level,
						   gboolean          show_critical);

void                mg_gantt_print_data_free      (MgGanttPrintData *data);


#endif /* __MG_GANTT_PRINT_JOB_H__ */

