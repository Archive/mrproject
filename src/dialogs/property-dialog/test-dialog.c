/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gtk/gtk.h>
#include <mrproject/mrproject.h>
#include "mg-property-dialog.h"

enum {
	COL_NAME,
	COL_OBJECT,
	COL_LAST
};

static void
cell_data_func (GtkTreeViewColumn *tree_column,
		GtkCellRenderer   *cell,
		GtkTreeModel      *tree_model,
		GtkTreeIter       *iter,
		gpointer           data)
{
	MrpObject   *object;
	MrpProperty *property = data;
	gchar       *str = NULL;

	gtk_tree_model_get (tree_model,
			    iter,
			    COL_OBJECT,
			    &object,
			    -1);

	mrp_object_get (object,
			mrp_property_get_name (property), &str,
			NULL);

	g_object_set (cell, "text", str, NULL);

	g_free (str);

}

static void
property_added (MrpProject  *project,
		MrpProperty *property,
		GtkWidget   *dialog)
{
	GtkTreeView       *tree;
	GtkTreeViewColumn *col;
	GtkCellRenderer   *cell;
	
	tree = g_object_get_data (G_OBJECT (dialog), "tree");
		
	cell = gtk_cell_renderer_text_new ();
	
	col = gtk_tree_view_column_new ();

	gtk_tree_view_column_set_title (
		col, mrp_property_get_label (property));

	gtk_tree_view_column_pack_start (col, cell, TRUE);

	gtk_tree_view_append_column (tree, col);

	gtk_tree_view_column_set_cell_data_func (col,
						 cell,
						 cell_data_func,
						 property,
						 NULL);
}

static void
property_removed (MrpProject  *project,
		  MrpProperty *property,
		  GtkWidget   *dialog)
{
	g_print ("removed\n");
}

static GtkWidget *
test_dialog_new (MrpObject *object)
{
	MrpProject        *project;
	GtkWidget         *dialog;
	GtkWidget         *tree;
	GtkTreeModel      *model;
	GtkTreeIter        iter;
	GtkTreeViewColumn *col;
	GtkCellRenderer   *cell;
	
	dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	model = GTK_TREE_MODEL (gtk_list_store_new (2,
						    G_TYPE_STRING,
						    G_TYPE_POINTER));

	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model),
			    &iter,
			    COL_NAME, "Test",
			    COL_OBJECT, object,
			    -1);

	tree = gtk_tree_view_new_with_model (model);
	gtk_widget_show (tree);

	g_object_set_data (G_OBJECT (dialog), "tree", tree);
	
	cell = gtk_cell_renderer_text_new ();
	
	col = gtk_tree_view_column_new_with_attributes ("Name",
							cell,
							"text", COL_NAME,
							NULL);
	
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), col);

	gtk_container_add (GTK_CONTAINER (dialog), tree);
	
	g_object_get (G_OBJECT (object), "project", &project, NULL);

	g_signal_connect (project,
			  "property_added",
			  G_CALLBACK (property_added),
			  dialog);

	g_signal_connect (project,
			  "property_removed",
			  G_CALLBACK (property_removed),
			  dialog);

	return dialog;
}

int
main (int argc, char *argv[])
{
	GtkWidget  *w;
	MrpTask    *task;
	MrpProject *project;
	MrpApplication *app;
	
	gtk_init (&argc, &argv);

	app = g_object_new (MRP_TYPE_APPLICATION, NULL);
	project = mrp_project_new (app);
	task = g_object_new (MRP_TYPE_TASK, NULL);
	mrp_project_insert_task (project, NULL, 0, task);
		
	w = mg_property_dialog_new (project,
				    MRP_TYPE_TASK,
				    "Test properties");
	gtk_window_set_default_size (GTK_WINDOW (w), 500, 300);
	gtk_widget_show (w);
	g_signal_connect (w, "delete-event", gtk_main_quit, NULL);

	w = test_dialog_new (MRP_OBJECT (task));
	gtk_window_set_default_size (GTK_WINDOW (w), 500, 300);
	gtk_widget_show (w);
	g_signal_connect (w, "delete-event", gtk_main_quit, NULL);
	
	gtk_main ();
	
	return 0;
}
