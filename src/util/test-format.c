#include <glib.h>
#include "mg-format.h"


int
main (int argc, char *argv[])
{
	float f[] = {
		-0.999297,
		-2,
		1.999775, 
		1.06,
		1.6
	};
	gint  i;

	for (i = 0; i < sizeof (f) / sizeof (float); i++) {
		g_print ("%10f: %4s\n",
			 f[i],
			 mg_format_float (f[i], 2, FALSE));
	}
			 
	return 0;
}
